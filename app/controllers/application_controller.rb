class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
      

  include ApplicationHelper

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
  end 
  # 以下の1行は消さないでください
  def default_url_options() {instanceId: 'a0a83b6dc7ca80356dd8890a0bd64c2beb91b3f0bedcf609448d1a6bf42c4746', containerPort: '3000', languageName: 'rails'}.merge(super) end
end
